#!/usr/bin/awk
BEGIN{
# separates fields by comma
FS=","
# initialized count variables for each letter grade
countA=0
countB=0
countC=0
countD=0
countE=0
}
{
# conditional statemnts that add to respective count
if ($2 >= 90 && $2 <=100) {countA=countA+1}
else if ($2 >= 80 && $2 <=89) {countB=countB+1}
else if ($2 >= 70 && $2 <=79) {countC=countC+1}
else if ($2 >= 60 && $2 <=69) {countD=countD+1}
else if ($2 >=0 && $2 <=59) {countD=countD+1}
}
END{
# print statements for each grade count
print "A,"countA
print "B,"countB
print "C,"countC
print "D,"countD
print "E,"countE
}
