#!/usr/bin/awk
# separates each field by comma
BEGIN{FS=","}
{
# initialized variable total at 0
total=0
# conditional that checks if first field is ADD
if ($1=="ADD") {
# if true, adds each number to the total
for (i=2;i<=NF;i++) {total+=$i}
}
# conditional that checks if first field is SUB
else if ($1="SUB") {
# if true, subtracts each number from the total
for (i=2;i<=NF;i++) {total-=$i}
}
# prints total
print total
}
