#!/usr/bin/awk
BEGIN{
# separates fields by comma
FS=","
# initializes online and onsite counts to 0
onlineCount=0
onsiteCount=0
}
{
# conditional stataments that update count of online and onsite students
if ($3=="online") {onlineCount++}
else if ($3=="onsite") {onsiteCount++}
}
# print statements for online and onsite counts
END{
print "online " onlineCount
print "onsite " onsiteCount
}
