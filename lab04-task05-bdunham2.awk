#!/usr/bin/awk
BEGIN{
# separates fields by comma
FS=","
# initialized variables for lowest and highest grade
# set the low grade to 100 to compare and change to lower grades later
lowestGrade=100
# set the high grade to 0 to compare and change to higher grades later
highestGrade=0
}

{
# if statement that evaluates if student is online
if ($3=="online") {
# if true, then grades of student are added to toal
onlineTotal+=$2
# count of online students is increased by
onlineCount++}

# else if statement that evaluates if student is onsite
else if ($3=="onsite") {
# if true, then grades of student are added to total
onsiteTotal+=$2
# count of onsite students is increased by 1
onsiteCount++}

# if statement to ignore "Final Grade" in first row
if ($2=="Final Grade") {lowestGrade+=0}
# if $2 is a lower grade, it becomes the new value of lowestGrade
else if ($2 < lowestGrade) {lowestGrade=$2}
# if $2 is a higher grade, it becomes the new value of highestGrade
else if ($2 > highestGrade) {highestGrade=$2}
}

END{
# calculated the average grades for both online and onsite students
onlineAverage=onlineTotal/onlineCount
onsiteAverage=onsiteTotal/onsiteCount
# prints section averages
print "Section Average"
# prints averages to 2 decimal places 
printf "online %.2f\n", onlineAverage
printf "onsite %.2f\n", onsiteAverage
# print statements for highest and lowest grades
print "Highest grade: " highestGrade
print "Lowest grade: " lowestGrade
}
